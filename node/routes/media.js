'use strict'

var express = require('express');
var MediaController = require('../controllers/media');

var md_auth = require('../middlewares/authenticated');
var api = express.Router();

// Subida imagen

var multipart = require('connect-multiparty');
var md_upload = multipart({uploadDir: 'uploads/media'});



api.get('/media/:slug',md_auth.ensureAuth, MediaController.getMedia);
api.get('/medias/:page?',md_auth.ensureAuth, MediaController.getMedias);
api.get('/my-medias/:page?',md_auth.ensureAuth, MediaController.getMyMedias);
api.get('/medias/:style/:page?',md_auth.ensureAuth, MediaController.getMediasByStyle);
api.get('/get-media/:mediaFile', MediaController.getMediaFile);



api.post('/upload-media',md_auth.ensureAuth, MediaController.saveMedia);
api.post('/upload-video/:id',[md_auth.ensureAuth, md_upload], MediaController.uploadMedia);


module.exports = api;