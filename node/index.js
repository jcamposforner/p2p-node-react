'use strict'

var mongoose = require('mongoose');
var app = require('./app');
var port = 3800;

// Conexion base de datos
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/p2p', {useNewUrlParser: true})
    .then(() => {
        console.log('Conexión a la BD establecida');

        // Crear servidor

        app.listen(port, () => {
            console.log('Servidor corriendo en http://localhost:'+port);
        })
    })
    .catch(err => console.log(err));
