'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var VotesSchema = Schema({
    user:{ type: Schema.ObjectId, ref: 'User' },
    media: { type: Schema.ObjectId, ref: 'Media' },
    rated: Number,
});


module.exports = mongoose.model('Votes', VotesSchema);