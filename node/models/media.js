'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MediaSchema = Schema({
    user:{ type: Schema.ObjectId, ref: 'User' },
    type: String,
    media: String,
    title: String,
    description: String,
    slug: String,
    activated: Boolean,
    style: String,
    created_at: String,
});


module.exports = mongoose.model('Media', MediaSchema);