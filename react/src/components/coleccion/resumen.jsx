import React,{Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';


const styles = theme => ({
    main: {
      width: 'auto',
      marginTop:theme.spacing.unit*2,
      display: 'block', // Fix IE 11 issue.
      marginLeft: theme.spacing.unit * 3,
      marginRight: theme.spacing.unit * 3,
      [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
    content: {
        marginTop:theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit * 3,
    },
    root: {
        flexGrow: 1,
      },
      paper: {
        height: 140,
        width: 100,
      },
      control: {
        padding: theme.spacing.unit * 2,
      },
  });

class Resumen extends Component {

    // solo 4 canciones en resumen
    
    render() {
        const { classes,name } = this.props;
        console.log(name)
        return (
            <div>
                <Grid 
                    container 
                    justify="center"
                    >
                    <Grid item xs={10}>
                        <Typography className={classes.content} paragraph color="textSecondary" component="h2" variant="h6" align="left">
                            {name.category}
                        </Typography>
                        
                    </Grid>
                </Grid>
                <div className="container">
                    <div className="columns">
                    { name.songs.map((song) =>
                        
                        
                        <div className="column is-3">
                            <div class="card">
                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <p class="title is-4">{song.title}</p>
                                            <p class="subtitle is-6">@{song.author}</p>
                                        </div>
                                    </div>

                                    <div class="content">
                                    {song.description}
                                    </div>
                                    <Link to={"/cancion/"+song.slug}>
                                        <Button variant="contained" className={classes.button}>
                                            Escuchar {song.title}
                                        </Button>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    )}
                    </div>
                </div>
            </div>
            

        );



    }



}

export default withStyles(styles)(Resumen);