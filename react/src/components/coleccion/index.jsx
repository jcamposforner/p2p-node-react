import React,{Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';
import AudioPlayer from "react-h5-audio-player";
import { BrowserRouter as Router, Route, NavLink, Redirect, withRouter,Switch } from 'react-router-dom';
import Resumen from './resumen';


const styles = theme => ({
    main: {
      width: 'auto',
      marginTop:theme.spacing.unit*2,
      display: 'block', // Fix IE 11 issue.
      marginLeft: theme.spacing.unit * 3,
      marginRight: theme.spacing.unit * 3,
      [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
    content: {
        marginTop:theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit * 3,
    },
    root: {
        flexGrow: 1,
      },
      paper: {
        height: 140,
        width: 100,
      },
      control: {
        padding: theme.spacing.unit * 2,
      },
  });

class Coleccion extends Component {
    state = {
        categories: [{
            category:'Escuchado recientemente',
            songs:[{
                title:'Primera cancion',
                author:'Jesus',
                description:'Primera descripcion',
                slug:'cancion1'
            },{
                title:'Primera cancion',
                author:'Jesus',
                description:'Primera descripcion',
                slug:'cancion2'
            },{
                title:'Primera cancion',
                author:'Jesus',
                description:'Primera descripcion',
                slug:'cancion3'
            },{
                title:'Primera cancion',
                author:'Jesus',
                description:'Primera descripcion',
                slug:'cancion4'
            }]
        },
        {
            category:'Me gusta',
            songs:[{}]
        },
        {
            category:'Listas',
            songs:[{}]
        }
        ,{
            category:'Siguiendo',
            songs:[{}]
        }],
    }

    render() {
        const { classes } = this.props;
        const { categories } = this.state;

        
        return (

            <main className={classes.main}>
            <CssBaseline />
                <Grid 
                    container 
                    justify="center"
                    >
                    <Grid item xs={10}>
                        <Grid
                        container
                        justify="left" 
                        alignItems="left" 
                        >
                            <Typography className={classes.content} paragraph color="textSecondary" component="h2" variant="h5" align="center">
                                <NavLink className="colection-links" to="/resumen">Resumen</NavLink>
                            </Typography>
                            <Typography className={classes.content} paragraph color="textSecondary" component="h2" variant="h5" align="center">
                                <NavLink className="colection-links" to="/megusta">Me gusta</NavLink>
                            </Typography>
                            <Typography className={classes.content} paragraph color="textSecondary" component="h2" variant="h5" align="center">
                                <NavLink className="colection-links" to="/listas">Listas</NavLink>
                            </Typography>
                            <Typography className={classes.content} paragraph color="textSecondary" component="h2" variant="h5" align="center">
                                <NavLink className="colection-links" to="/siguiendo">Siguiendo</NavLink>
                            </Typography>
                            <Typography className={classes.content} paragraph color="textSecondary" component="h2" variant="h5" align="center">
                                <NavLink className="colection-links" to="/historial">Historial</NavLink>
                            </Typography>
                        </Grid>                        
                    </Grid>
                    
                    
                </Grid>

                { categories.map((category) =>
                    <Resumen name={category}></Resumen>
                    )}


                <AudioPlayer
                        src="uploads/0UmuXjSgQz_-SD3Lrtz0s_vi.mp3"
                        onPlay={e => console.log("onPlay")}
                        onEnded={e => console.log("onEnded")}

                    />
            </main>

        );



    }



}

export default withStyles(styles)(Coleccion);