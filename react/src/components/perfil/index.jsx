import React,{Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { urlApi } from '../../env'
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import FavoriteIcon from '@material-ui/icons/Favorite';
import AudioPlayer from "react-h5-audio-player";


const styles = theme => ({
    main: {
      width: 'auto',
      display: 'block', // Fix IE 11 issue.
      marginLeft: theme.spacing.unit * 3,
      marginTop: theme.spacing.unit *3,
      marginRight: theme.spacing.unit * 3,
      [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
      },
    },
    paper: {
      marginTop: theme.spacing.unit * 8,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
      margin: theme.spacing.unit,
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing.unit,
    },
    submit: {
      marginTop: theme.spacing.unit * 3,
    },
    avatar: {
        margin: 10,
      },
      bigAvatar: {
        margin: 40,
        width: 240,
        height: 240,
      },
  });


class Perfil extends Component {

    constructor(props){
        super(props)
        this.state = {
            type:'',
            identity:JSON.parse(localStorage.getItem('identity')),
            token:JSON.parse(localStorage.getItem('token')),
            medias:[]
        };
    }

    async componentWillMount(){
        await axios.get(`${urlApi}my-medias`, {
            headers : {
               'Content-Type':'application/json',
               'Authorization': this.state.token
               }
           }).then(
               (res) => {
                   this.setState({
                       medias: res.data.medias,
                       currentPage: res.data.currentPage,
                       total: res.data.total,
                       pages: res.data.pages,
                   })
               },(err) =>{
                   console.log(err)
               }
       )
    }

    
    render (){
        const { classes } = this.props;
        const { identity,medias,currentPage,total,pages } = this.state;
        console.log(medias)
        return (
            <div>
                <div className="columns">
                    <div style={{backgroundImage: "url(uploads/fondo-degradado.jpg)"}} className="column backgroundImage is-12">
                    
                        <div className="columns">
                            <div className="column is-4">
                                
                                <Avatar alt={this.state.identity._id} src={urlApi+"get-image-user/"+this.state.identity.image} className={classes.bigAvatar} />
                                
                            </div>

                            <div className="column is-4 flexbox">
                            <br></br><br></br><br></br><br></br>
                                <Typography component="h2" variant="h3" align="center" color="textPrimary">
                                    {this.state.identity.nick}
                                </Typography>
                                <Typography component="h2" variant="h5" align="center" color="inherit">
                                    {this.state.identity.name} {this.state.identity.surname}
                                </Typography>

                            </div>
                        </div>

                    </div>
                </div>
                <div className="container">
                    <div className="column is-12">
                        <Typography component="h2" variant="h3" align="left" color="inherit">
                            Mis subidas ({total})
                        </Typography>
                    </div>
                    <div className="columns">
                        <div className="column is-8">

                        { 
                            medias.map((media) =>
                            
                                <div key={media._id} className="card">
                                
                                    <header className="card-header">
                                        <p className="card-header-title">
                                            {media.title} -- {media.style} -
                                        </p>
                                        
                                    </header>
                                    <div className="card-content">
                                        <div className="content">
                                            {media.description}
                                        
                                        </div>
                                        
                                    </div>
                                    {
                                        media.type == 'Video' ? 
                                        
                                        null
                                        
                                        :
                                    <div>
                                    {media.type}
                                    <AudioPlayer
                                        controls
                                        src={urlApi+"get-media/"+media.media}
                                        onPlay={e => console.log("onPlay")}
                                    />
                                    </div>
                                    }
                                    <BottomNavigation>
                                        <BottomNavigationAction label="Favorites" icon={<FavoriteIcon />} />
                                    </BottomNavigation>
                                <hr></hr>
                                </div>
                            )}

                        </div>
                        
                        <div className="column is-4">
                        <div className="card seguidores">
                                
                                <footer className="card-footer">
                                    <p className="card-footer-item">
                                        Seguidores <br /> <b>0</b>
                                    </p>
                                    <p className="card-footer-item">
                                        Siguiendo <br /> <b>0</b>
                                    </p>
                                    <p className="card-footer-item">
                                        Media <br /> <b>{total}</b>
                                    </p>
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    };
}
Perfil.propTypes = {
    classes: PropTypes.object.isRequired,
  };


export default 
    withStyles(styles)(
    (withRouter(Perfil))
);