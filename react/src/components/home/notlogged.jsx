import React,{Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';
import { Link } from 'react-router-dom';


const styles = theme => ({
    main: {
      width: 'auto',
      marginTop:theme.spacing.unit*8,
      display: 'block', // Fix IE 11 issue.
      marginLeft: theme.spacing.unit * 3,
      marginRight: theme.spacing.unit * 3,
      [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
    content: {
        marginTop:theme.spacing.unit * 2,
    },
    root: {
        flexGrow: 1,
      },
      paper: {
        height: 140,
        width: 100,
      },
      control: {
        padding: theme.spacing.unit * 2,
      },
  });

class HomeNotLogged extends Component {

    render() {
        const { classes } = this.props;
        return (

            <main className={classes.main}>
            <CssBaseline />
                <Grid 
                    container 
                    justify="center"
                    >
                    <Grid item xs={8}>
                        <Grid
                        container
                        justify="center" 
                        alignItems="center" 
                        >
                            <Typography component="h1" variant="h2" align="center" color="textPrimary">
                                ¡Comparte tu música!
                            </Typography>
                            <Typography className={classes.content} variant="h6" align="center" color="textSecondary" paragraph>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.


                            </Typography>
                        </Grid>

                        <Grid item xs={12}>
                            <img className="banner-not-logged" src="assets/music.png" width="40%" alt="banner" />
                        </Grid>

                        <Grid container className={classes.root} spacing={16}>
                            <Grid item xs={12}>
                                <Grid container className={classes.demo} justify="center" spacing={40}>
                                    
                                    <Grid item>
                                        <Link to="/login"><Button variant="contained" color="primary">Iniciar Sesión</Button></Link>
                                    </Grid>
                                    <Grid item>
                                        <Link to="/registro"><Button variant="contained" color="secondary" >Registrarse</Button></Link>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        
                        
                    </Grid>
                    
                </Grid>
            </main>

        );



    }



}

export default withStyles(styles)(HomeNotLogged);