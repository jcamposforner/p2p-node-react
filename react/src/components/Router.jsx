import React, {Component} from 'react';
import { BrowserRouter as Router, Route, Link, Redirect, withRouter,Switch } from 'react-router-dom';
import NavBar from './header/navbar';
import Footer from './footer';
import Login from './login/index';
import Registro from './register/index';
import HomeNotLogged from './home/notlogged';
import HomeLogged from './home/logged';
import Coleccion from './coleccion/'
import Cancion from './cancion';
import UploadMedia from './upload';
import MyAccount from './account';
import Perfil from './perfil';

// PARA QUE NO TENGA HEADER EL ADMIN
const DefaultRouter = () => (
    <Router>
        <Switch>
            <Route component={StaticSite} />
        </Switch>
    </Router>
)


const StaticSite = () => (
            <Router>
                <div>
                    <NavBar></NavBar>
                    <Switch>
                        <Route exact path="/" component={HomeComponent} />
                        <Route exact path="/resumen" component={ColeccionComponent} />
                        <Route exact path="/mi-perfil" component={PerfilComponent} />
                        <Route exact path="/cancion/:slug" component={CancionComponent} />
                        <Route exact path="/login" component={LoginComponent} />
                        <Route exact path="/my-account" component={MyAccountComponent} />
                        <Route exact path="/upload" component={UploadComponent} />
                        <Route exact path="/registro" component={RegistroComponent} />
                    </Switch>
                    <Footer></Footer>
                </div>

            </Router>
)

const HomeComponent = () => (
    localStorage.getItem('auth') ? (
        <HomeLogged></HomeLogged>
    ):<HomeNotLogged></HomeNotLogged>
)

const MyAccountComponent = () => (
    localStorage.getItem('auth') ? (
        <MyAccount></MyAccount>
    ):<Login></Login>
)

const UploadComponent = () => (
    localStorage.getItem('auth') ? (
        <UploadMedia></UploadMedia>
    ):<HomeNotLogged></HomeNotLogged>
    
)

const PerfilComponent = () => (
    localStorage.getItem('auth') ? (
        <Perfil></Perfil>
    ):<Login></Login>
)

const CancionComponent = (match) => (
    localStorage.getItem('auth') ? (
        <Cancion slug={match.match.params.slug}></Cancion>
    ):<Redirect to="/login"></Redirect>
)

const ColeccionComponent = () => (
    localStorage.getItem('auth') ? (
        <Coleccion></Coleccion>
    ):<Redirect to="/login"></Redirect>
)

const LoginComponent = () => (
    localStorage.getItem('auth') ? (
    
    <Redirect to="/"></Redirect>
    ):
    <Login></Login>
)

const RegistroComponent = () => (
    localStorage.getItem('auth') ? (
    
        <Redirect to="/"></Redirect>
        ):
        <Registro></Registro>
)

export default DefaultRouter;