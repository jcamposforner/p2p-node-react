import React,{Component} from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import LockIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { urlApi } from '../../env'

const styles = theme => ({
    main: {
      width: 'auto',
      display: 'block', // Fix IE 11 issue.
      marginLeft: theme.spacing.unit * 3,
      marginRight: theme.spacing.unit * 3,
      [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
        width: 400,
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
    paper: {
      marginTop: theme.spacing.unit * 8,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
      margin: theme.spacing.unit,
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing.unit,
    },
    submit: {
      marginTop: theme.spacing.unit * 3,
    },
  });


class MyAccount extends Component {

    constructor(props){
        super(props)
        this.state = {
            type:'',
            identity:JSON.parse(localStorage.getItem('identity')),
            token:JSON.parse(localStorage.getItem('token')),
        };
    }

    
    handleSubmit(e) {
        e.preventDefault();
        const formData = {
            name: document.getElementById("name").value,
            surname: document.getElementById("surname").value,
            nick: document.getElementById("nick").value,
            password: document.getElementById("password").value,
        }
        console.log(formData)

        axios.put(`${urlApi}update-user/${this.state.identity._id}`,formData, {
            headers : {
               'Content-Type':'application/json',
               'Authorization': this.state.token
               }
           }).then(
               (res) => {
                   this.fileUploadHandler();
               },(err) =>{
                   console.log(err)
               }
       )
        

    }

    fileUploadHandler = () => {
        const fd = new FormData();
        fd.append('image', this.state.selectedFile, this.state.selectedFile.name);
        axios.post(`${urlApi}upload-image-user/${this.state.identity._id}`,fd, {
            headers : {
               'Content-Type':'application/json',
               'Authorization': this.state.token
               }
           }).then(
               (res) => {
                   console.log(res.data)
               },(err) =>{
                   console.log(err)
               }
       )
    }

    fileSelectedHandler = event => {
        this.setState({
            selectedFile: event.target.files[0]
        })
    }


    render (){
        const { classes } = this.props;
        const { identity } = this.state;
        return (
            <main className={classes.main+" centered"}>
            <CssBaseline />
            <Paper className={classes.paper}>
                <Avatar className={classes.avatar}>
                <LockIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Tus datos personales
                </Typography>
                <hr />
                    <form onSubmit={this.handleSubmit.bind(this)} className={classes.form}>
                        <FormControl margin="normal" required fullWidth>
                            <InputLabel htmlFor="name">Nombre</InputLabel>
                            <Input placeholder={identity.name} id="name" name="name" autoComplete="name" autoFocus />
                        </FormControl>
                        <FormControl margin="normal" required fullWidth>
                            <InputLabel htmlFor="surname">Apellidos</InputLabel>
                            <Input placeholder={identity.surname} id="surname" name="surname" autoComplete="surname" />
                        </FormControl>
                        <FormControl margin="normal" required fullWidth>
                            <InputLabel htmlFor="nick">Nick</InputLabel>
                            <Input placeholder={identity.nick} id="nick" name="nick" autoComplete="nick" />
                        </FormControl>
                        <FormControl margin="normal" required fullWidth>
                            <InputLabel htmlFor="password">Contraseña</InputLabel>
                            <Input name="password" type="password" id="password" autoComplete="current-password" />
                        </FormControl>
                        <FormControl margin="normal" required fullWidth>
                            <InputLabel htmlFor="image">Imagen</InputLabel>
                            <Input onChange={this.fileSelectedHandler.bind(this)} name="image" type="file" id="image" autoComplete="image" />
                        </FormControl>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                        >
                        Cambiar Datos
                    </Button>
                </form>
                
                </Paper>
            </main>
        );
    };
}
MyAccount.propTypes = {
    classes: PropTypes.object.isRequired,
  };


export default 
    withStyles(styles)(
    (withRouter(MyAccount))
);