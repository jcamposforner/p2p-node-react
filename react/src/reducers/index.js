import {combineReducers} from 'redux';
import ReducerUser from './reducer-user';

const allReducers = combineReducers({
    ReducerUser: ReducerUser
});

export default allReducers;