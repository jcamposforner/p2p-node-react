export default function (state=null, action) {
    switch (action.type) {
        case "USER_LOGGED":
            localStorage.setItem ('auth',true);
            return {
                ...state,
                auth:true,
            }
        case "USER_NOT_LOGGED":
            return {
                ...state,
                auth:false,
            }
    }
    return state;
}